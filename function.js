
console.log('Задание 1');

var apples = 20; // Яблоки, кг
var strawberry = 20; // Клубника - 75 р/кг
var apricot = 20; // Абрикос - 35 р/кг
var flour = 20; // Мука - 10 р/кг
var milk = 20; // Молоко - 15 р/литр
var eggs = 50; // Яйца - 3 р/шт
var applePie = 0; // количество яблочных пирогов, которое испекли.
var strawberryPie = 0; // количество клубничных пирогов, которое испекли.
var apricotPie = 0; // количество абрикосовых пирогов, которое испекли.

function bake(appleCount, strawberryCount, apricotCount, flourCount, milkCount, eggCount) {
    if (apples - appleCount < 0) {
        console.log('Кончились яблоки! Купили ещё 10 кг');
        apples += 10;
    } else if (strawberry - strawberryCount < 0) {
        console.log('Кончилась клубника! Купили ещё 10 кг');
        strawberry += 10;
    } else if (apricot - apricotCount < 0) {
        console.log('Кончились абрикосы! Купили ещё 10 кг');
        apricot += 10;
    } else if (flour - flourCount < 0) {
        console.log('Кончилась мука! Купили еще 10 кг');
        flour = flour + 10;
    } else if (milk - milkCount < 0) {
        console.log('Кончилось молоко! Купили еще 10 литров');
        milk = milk + 10;
    } else if (eggs - eggCount < 0) {
        console.log('Кончились яйца! Купили еще 10 штук');
        eggs = eggs + 10;
    } else {
        apples = apples - appleCount;
        flour = flour - flourCount;
        milk = milk - milkCount;
        eggs = eggs - eggCount;
    }

    if (appleCount > 0) {
        applePie++;
        console.log('Яблочный пирог ' +applePie+ ' готов!');
    } else if (strawberryCount > 0) {
        strawberryPie++;
        console.log('Клубничный пирог ' +strawberryPie+ ' готов!');
    } else if (apricotCount > 0) {
        apricotPie++;
        console.log('Абрикосовый пирог '+apricotPie+' готов!');
    } else {
        console.log('Неверный рецепт!')
    }
}

while (applePie < 10) {
    bake(3, 0, 0, 2, 1, 3);
}

while (strawberryPie < 10) {
    bake(0, 5, 0, 1, 2, 4);
}

while ( apricotPie < 10) {
    bake(0, 0, 2, 3, 2, 2);
}

console.log('Задание 2');

function sumTo1(n) {
    if (n == 1) {
        return 1;
    } else if (n > 0) {
        return n + sumTo1(n - 1);
    } else {
        return 'Число n должно быть больше нуля!';
    }
}

console.log (sumTo1(5));


function sumTo2(n) {
    var sum = 0;
    for (var i = 1; i <= n; i++) {
        sum += i;
    }
    return sum;
}

console.log(sumTo2(6));
